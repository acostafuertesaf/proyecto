import * as React from 'react';
import Layout from '../../../components/LayoutParametrizacion'
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';



const columns = [
  { id: 'tipo', label: 'Tipo ahorro', minWidth: 100 },
  { id: 'usuario', label: 'Usuario', minWidth: 100 },
  { id: 'valorTotalAhorrado', label: 'Valor total ahorrado', minWidth: 100 },
  { id: 'fechaInicioAhorro', label: 'Fecha inicio ahorro', minWidth: 100 },
  { id: 'fechaRetiroAhorro', label: 'Fecha retiro ahorro', minWidth: 100 },
  { id: '', label: 'Acciones', minWidth: 100 }
  
  /* {
    id: 'population',
    label: 'Population',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  }, */
];

function createData(tipo, usuario, valorTotalAhorrado, fechaInicioAhorro, fechaRetiroAhorro) {
  return { tipo, usuario, valorTotalAhorrado, fechaInicioAhorro, fechaRetiroAhorro};
}


const rows = [
 /*  createData('India', 'IN', 1324171354), */
];


export default function SocioPage() {

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  
  return (
    <Layout subMenu="Ahorro" add="agregarAhorro" viewAdd={true}>
      <Paper sx={{ width: '100%', overflow: 'hidden' }}>
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === 'number'
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 20]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Layout>
  );
}