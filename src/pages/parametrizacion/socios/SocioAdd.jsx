import * as React from 'react';
import Layout from '../../../components/LayoutParametrizacion'
import Paper from '@mui/material/Paper';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import SaveIcon from '@mui/icons-material/Save';
import InputAdornment from '@mui/material/InputAdornment';

const top100Films = [
  { label: 'The Shawshank Redemption', year: 1994 },
  { label: 'The Godfather', year: 1972 },
  { label: 'The Godfather: Part II', year: 1974 },
  { label: 'The Dark Knight', year: 2008 },
  { label: '12 Angry Men', year: 1957 },
  { label: "Schindler's List", year: 1993 },

];

export default function Add() {

  const [empleado, setEmpleado] = React.useState("");
  const [porcentaje, setPorcentaje] = React.useState("");


  return (
    <Layout subMenu="Agregar socios" >
      <Container component="main" maxWidth="sm">
        <Paper sx={{ width: '100%', overflow: 'hidden', padding: '5%' }} elevation={10}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={12}>
              <Autocomplete
                disablePortal
                id="Empleados"
                options={top100Films}
                getOptionLabel={(option) => option.label}
                size={"small"}
                fullWidth={true}
                renderInput={(params) => <TextField {...params} label="Empleados" />}
                onChange={(event, value) => {
                  setEmpleado(value ? value.perfil : "")
                }}
              />
            </Grid>

            <Grid item xs={12} md={12}>
              <TextField
                value={porcentaje}
                id="porcentaje"
                label="Porcentaje ahorro voluntario"
                variant="outlined"
                size={"small"}
                fullWidth={true}
                InputProps={{
                  endAdornment: <InputAdornment position="start">%</InputAdornment>,
                }}
                onChange={(e) => {
                  setPorcentaje(e.target.value.replace(/[^0-9,.]/gi, ''))
                }}
              />
            </Grid>

            <Grid item xs={12} md={12} >
              <Grid item xs={12} md={6} style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                <Button variant="contained" size="mediun" fullWidth >
                  Guardar  <SaveIcon style={{ marginLeft: '10px' }} />
                </Button>
              </Grid>
            </Grid>
          </Grid>

        </Paper>
      </Container>
    </Layout >
  );
}