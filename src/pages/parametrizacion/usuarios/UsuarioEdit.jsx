import * as React from 'react';
import Layout from '../../../components/LayoutParametrizacion'
import Paper from '@mui/material/Paper';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import SaveIcon from '@mui/icons-material/Save';

const top100Films = [
  { label: 'The Shawshank Redemption', year: 1994 },
  { label: 'The Godfather', year: 1972 },
  { label: 'The Godfather: Part II', year: 1974 },
  { label: 'The Dark Knight', year: 2008 },
  { label: '12 Angry Men', year: 1957 },
  { label: "Schindler's List", year: 1993 },

];

export default function Edit() {
  const [values, setValues] = React.useState({
    password: '',
    showPassword: false,
  });

  const [perfil, setPerfil] = React.useState("");
  const [empleado, setEmpleado] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [contraseña, setContraseña] = React.useState("");

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  return (
    <Layout subMenu="Editar usuarios" >
      <Container component="main" maxWidth="sm">
        <Paper sx={{ width: '100%', overflow: 'hidden', padding: '5%' }} elevation={10}>

          <Grid container spacing={2}>
            <Grid item xs={12} md={12}>
              <Autocomplete
                disablePortal
                id="perfiles"
                options={top100Films}
                getOptionLabel={(option) => option.label}
                size={"small"}
                fullWidth={true}
                renderInput={(params) => <TextField {...params} label="Perfiles" />}
                onChange={(event, value) => {
                  setPerfil(value ? value.perfil : "")
                }}
              />

            </Grid>

            <Grid item xs={12} md={12}>
              <Autocomplete
                disablePortal
                id="Empleados"
                options={top100Films}
                getOptionLabel={(option) => option.label}
                size={"small"}
                fullWidth={true}
                renderInput={(params) => <TextField {...params} label="Empleados" />}
                onChange={(event, value) => {
                  setEmpleado(value ? value.perfil : "")
                }}
              />

            </Grid>

            <Grid item xs={12} md={12}>
              <TextField
                value={email}
                id="email"
                label="Correo electrónico"
                variant="outlined"
                size={"small"}
                fullWidth={true}
                onChange={(e) => {
                  setEmail(e.target.value)
                }}
              />
            </Grid>
            <Grid item xs={12} md={12}>
              <TextField
                value={contraseña}
                id="contraseña"
                label="Contraseña"
                variant="outlined"
                size={"small"}
                fullWidth={true}
                type="password"
                onChange={(e) => {
                  setContraseña(e.target.value)
                }}
              />
            </Grid>

            <Grid item xs={12} md={12} >
              <Grid item xs={12} md={6} style={{marginLeft: 'auto', marginRight: 'auto'}}>
                <Button variant="contained" size="mediun" fullWidth >
                  Guardar  <SaveIcon style={{marginLeft: '10px'}}/>
                </Button>
              </Grid>
            </Grid>


          </Grid>


        </Paper>
      </Container>
    </Layout>
  );
}