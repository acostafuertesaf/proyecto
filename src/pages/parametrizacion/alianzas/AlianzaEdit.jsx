import * as React from 'react';
import Layout from '../../../components/LayoutParametrizacion'
import Paper from '@mui/material/Paper';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import SaveIcon from '@mui/icons-material/Save';
import InputAdornment from '@mui/material/InputAdornment';

const top100Films = [
  { label: 'The Shawshank Redemption', year: 1994 },
  { label: 'The Godfather', year: 1972 },
  { label: 'The Godfather: Part II', year: 1974 },
  { label: 'The Dark Knight', year: 2008 },
  { label: '12 Angry Men', year: 1957 },
  { label: "Schindler's List", year: 1993 },

];

export default function Edit() {

  const [tipo, setTipo] = React.useState("");
  const [nombre, setNombre] = React.useState("");
  const [objetivo, setObjetivo] = React.useState("");
  const [observaciones, setObservaciones] = React.useState("");


  return (
    <Layout subMenu="Editar alianza" >
      <Container component="main" maxWidth="sm">
        <Paper sx={{ width: '100%', overflow: 'hidden', padding: '5%' }} elevation={10}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={12}>
              <Autocomplete
                disablePortal
                id="tipoAlianza"
                options={top100Films}
                getOptionLabel={(option) => option.label}
                size={"small"}
                fullWidth={true}
                renderInput={(params) => <TextField {...params} label="Tipo alianza" />}
                onChange={(event, value) => {
                  setTipo(value ? value.perfil : "")
                }}
              />
            </Grid>

            <Grid item xs={12} md={12}>
              <TextField
                value={nombre}
                id="nombre"
                label="Nombre"
                variant="outlined"
                size={"small"}
                fullWidth={true}
                onChange={(e) => {
                  setNombre(e.target.value)
                }}
              />
            </Grid>

            <Grid item xs={12} md={12}>
              <TextField
                value={objetivo}
                id="objetivo"
                label="Objetivo"
                variant="outlined"
                size={"small"}
                fullWidth={true}
                onChange={(e) => {
                  setObjetivo(e.target.value)
                }}
              />
            </Grid>

            <Grid item xs={12} md={12}>
              <TextField
                value={observaciones}
                id="observaciones"
                label="Observaciones"
                variant="outlined"
                size={"small"}
                multiline
                rows={4}
                fullWidth={true}
                onChange={(e) => {
                  setObservaciones(e.target.value)
                }}
              />
            </Grid>

            <Grid item xs={12} md={12} >
              <Grid item xs={12} md={6} style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                <Button variant="contained" size="mediun" fullWidth >
                  Guardar  <SaveIcon style={{ marginLeft: '10px' }} />
                </Button>
              </Grid>
            </Grid>
          </Grid>

        </Paper>
      </Container>
    </Layout >
  );
}