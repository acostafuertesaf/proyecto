import * as React from 'react';
import Layout from '../../../components/LayoutParametrizacion'
import Paper from '@mui/material/Paper';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import SaveIcon from '@mui/icons-material/Save';
import InputAdornment from '@mui/material/InputAdornment';

const top100Films = [
  { label: 'The Shawshank Redemption', year: 1994 },
  { label: 'The Godfather', year: 1972 },
  { label: 'The Godfather: Part II', year: 1974 },
  { label: 'The Dark Knight', year: 2008 },
  { label: '12 Angry Men', year: 1957 },
  { label: "Schindler's List", year: 1993 },

];

export default function Add() {

  const [usuario, setUsuario] = React.useState("");
  const [valorDeuda, setValorDeuda] = React.useState("");
  const [valorCuota, setValorCuota] = React.useState("");
  const [fechaSolicitup, setFechaSolicitup] = React.useState("");
  const [tiempoEstimadoPago, setTiempoEstimadoPago] = React.useState("");


  return (
    <Layout subMenu="Editar credito libre inversión" >
      <Container component="main" maxWidth="sm">
        <Paper sx={{ width: '100%', overflow: 'hidden', padding: '5%' }} elevation={10}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={12}>
              <Autocomplete
                disablePortal
                id="usuario"
                options={top100Films}
                getOptionLabel={(option) => option.label}
                size={"small"}
                fullWidth={true}
                renderInput={(params) => <TextField {...params} label="Usuario" />}
                onChange={(event, value) => {
                  setUsuario(value ? value.nombre : "")
                }}
              />
            </Grid>

            <Grid item xs={12} md={12}>
              <TextField
                value={valorDeuda}
                id="valorDeuda"
                label="Valor deuda"
                variant="outlined"
                size={"small"}
                fullWidth={true}
                onChange={(e) => {
                  setValorDeuda(e.target.value.replace(/[^0-9,.]/gi, ''))
                }}
              />
            </Grid>

            <Grid item xs={12} md={12}>
              <TextField
                value={valorCuota}
                id="valorCuota"
                label="Valor cuota"
                variant="outlined"
                size={"small"}
                fullWidth={true}
                onChange={(e) => {
                  setValorCuota(e.target.value.replace(/[^0-9,.]/gi, ''))
                }}
              />
            </Grid>

            <Grid item xs={12} md={12}>
              <TextField
                value={fechaSolicitup}
                id="fechaSolicitup"
                label="Fecha Solicitup"
                variant="outlined"
                size={"small"}
                fullWidth={true}
                onChange={(e) => {
                  setFechaSolicitup(e.target.value)
                }}
              />
            </Grid>
            <Grid item xs={12} md={12}>
              <TextField
                value={tiempoEstimadoPago}
                id="tiempoEstimadoPago"
                label="Tiempo estimado pago"
                variant="outlined"
                size={"small"}
                fullWidth={true}
                onChange={(e) => {
                  setTiempoEstimadoPago(e.target.value)
                }}
              />
            </Grid>

            <Grid item xs={12} md={12} >
              <Grid item xs={12} md={6} style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                <Button variant="contained" size="mediun" fullWidth >
                  Guardar  <SaveIcon style={{ marginLeft: '10px' }} />
                </Button>
              </Grid>
            </Grid>
          </Grid>

        </Paper>
      </Container>
    </Layout >
  );
}