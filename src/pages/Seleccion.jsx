import React from 'react'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@mui/material/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@mui/material/Container';
import Copyright from '../components/Copyright'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import CardActions from '@mui/material/CardActions';
import Grid from '@mui/material/Grid';
import img from '../assets/images.jpg'
import Link from '@mui/material/Link';
import Logo from '../assets/logo.png'
import AppBar from '@mui/material/AppBar';
import AccountCircle from '@mui/icons-material/AccountCircle';
import Switch from '@mui/material/Switch';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import LogoutIcon from '@mui/icons-material/Logout';

import Toolbar from '@mui/material/Toolbar';


import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import MoreIcon from '@mui/icons-material/MoreVert';



const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',

  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  card: {
    width: '100%',
    borderRadius: '50px'
    //background: '-webkit-linear-gradient(white, red)',
    //WebkitBackgroundClip: 'text',
    //WebkitTextFillColor: 'transparent'
  },
  panel: {
    marginTop: '40px'
  }
}));

export default function Seleccion() {
  const classes = useStyles();

  const [auth, setAuth] = React.useState(true);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleChange = (event) => {
    setAuth(event.target.checked);
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };


  return (
    <React.Fragment>
      
      
      <AppBar position="static" style={{backgroundColor: '#fff'}}>
        <Toolbar>
          <img src={Logo} width='50' />
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }} style={{color: '#000'}}>
            Seleccionar opción
          </Typography>
          {auth && (
            <div>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                style={{color: '#000'}}
                //color="inherit"
              >
                <MoreIcon />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem onClick={handleClose}>Profile</MenuItem>
                <MenuItem onClick={handleClose}>My account</MenuItem>
              </Menu>
            </div>
          )}
        </Toolbar>
      </AppBar>
           
      <Container component="main" maxWidth="md" className={classes.panel}>
        <Grid container direction="row" justifyContent="center" alignItems="center" spacing={2}>
          <Grid item xs={4} >
            <Link href="/homeAdmin" underline="none">
              <Card  >
                <CardContent>
                  <Typography align={'center'} >
                    ADMINISTRACIÓN
                  </Typography>
                </CardContent>
              </Card>
            </Link>
          </Grid>

          <Grid item xs={4} >
            <Link href="/homeParametrizacion" underline="none">
              <Card className={classes.card}>
                <CardContent>
                  <Typography align={'center'}>
                    PARAMETRIZACÍON
                  </Typography>
                </CardContent>
              </Card>
            </Link>
          </Grid>



          <Grid item xs={4} >
            <Link href="/homeOperador" underline="none">
              <Card className={classes.card}>
                <CardContent>
                  <Typography align={'center'}>
                    OPERADOR
                  </Typography>
                </CardContent>
              </Card>
            </Link>
          </Grid>


        </Grid>


        <Box mt={8}><Copyright /></Box>
      </Container>
    </React.Fragment>
  );
}


