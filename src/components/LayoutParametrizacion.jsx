import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Link from '@mui/material/Link';
import GroupIcon from '@mui/icons-material/Group';
import Usuarios from '@mui/icons-material/Person';
import Opciones from '@mui/icons-material/ListAlt';
import Permisos from '@mui/icons-material/Lock';
import Organizaciones from '@mui/icons-material/HomeWork';
import Empleados from '@mui/icons-material/SwitchAccount';
import Perfiles from '@mui/icons-material/PlaylistAddCheck';
import Socios from '@mui/icons-material/AccountCircle';
import TipoAlianzas from '@mui/icons-material/GroupWork';
import TipoAplicaciones from '@mui/icons-material/Apps';
import KeyboardReturnIcon from '@mui/icons-material/KeyboardReturn';
import AddIcon from '@mui/icons-material/Add';
import HomeIcon from '@mui/icons-material/Home';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import SavingsIcon from '@mui/icons-material/Savings';
import CreditScoreIcon from '@mui/icons-material/CreditScore';
import SplitscreenIcon from '@mui/icons-material/Splitscreen';

const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  }),
);

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

const Lista = (title, icon, to) => {
  return (
    <>
      <Link href={`/${to}`} underline="none" color="inherit">
        <ListItem button>
          <ListItemIcon>
            {icon}
          </ListItemIcon>
          <ListItemText primary={title} />
        </ListItem>
      </Link>
      <Divider />
    </>
  );
}



export default function Layout(props) {
  const {
    subMenu,
    add,
    viewAdd,
  } = props;

  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar position="fixed" open={open}>

        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{ mr: 2, ...(open && { display: 'none' }) }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div">
            Parametrización
          </Typography>
          {
            subMenu !== "" ?
              <Typography style={{ marginLeft: '20px', borderBottom: '1px solid white' }} component="div">
                {subMenu}
              </Typography >
              :
              null
          }

          {
            viewAdd ?
              <Tooltip title="Agregar">
                <Button variant="outlined" href={`/${add}`} style={{ border: '1px solid white', color: 'white', marginLeft: '20px' }} size="small" >
                  <AddIcon />
                </Button>
              </Tooltip>
              : null
          }

          <Button variant="outlined" style={{ border: '1px solid white', color: 'white', marginLeft: 'auto', marginRight: '10px' }} size="small" href="seleccion">
            <HomeIcon />
          </Button>

          <Button variant="outlined" style={{ border: '1px solid white', color: 'white', marginLeft: 'auto', margin: '0px' }} size="small" >
            Cerrar sesión
          </Button>



        </Toolbar>

      </AppBar>

      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="left"
        open={open}
      >
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </DrawerHeader>
        <Divider />

        <List>
          {Lista("Usuarios", <Usuarios />, "usuarios")}
          {Lista("Socios", <Socios />, "socios")}
          {Lista("Tipo aplicaciones", <TipoAplicaciones />, "tipoAplicacion")}
          {Lista("Tipo alianzas", <TipoAlianzas />, "tipoAlianza")}
          {Lista("Tipo ahorro", <SplitscreenIcon />, "tipoAhorro")}
          {Lista("Alianzas", <GroupIcon />, "alianza")}
          {Lista("Ahorros", <SavingsIcon />, "ahorro")}
          {Lista("Credito libre i.", <CreditScoreIcon />, "credito")}
          {Lista("Empleados", <Empleados />, "empleado")}
          {Lista("Organizaciones", <Organizaciones />, "organizaciones")}
          {Lista("Perfiles", <Perfiles />, "perfiles")}
          {Lista("Permisos", <Permisos />, "permisos")}
          {Lista("Opciones", <Opciones />, "opciones")}
          {/* {Lista("Departamento", <GroupIcon />)}
          {Lista("Municipio", <GroupIcon />)} */}


        </List>
        <Divider />
      </Drawer>

      <Main open={open}>
        <DrawerHeader />
        {props.children}
      </Main>
    </Box>
  );
}