import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

/* Pages */
import Login from './containers/Login'
import Seleccion from './containers/Seleccion'


/* Admin */
import HomeAdmin from './pages/admin/Home';


/* Parametrizacion */
import HomeParametrizacion from './pages/parametrizacion/Home';
import Usuario from './containers/parametrizacion/usuarios/Usuario';
import UsuarioAdd from './containers/parametrizacion/usuarios/UsuarioAdd';
import UsuarioEdit from './containers/parametrizacion/usuarios/UsuarioEdit';
import Socio from './containers/parametrizacion/socios/Socio';
import SocioAdd from './containers/parametrizacion/socios/SocioAdd';
import SocioEdit from './containers/parametrizacion/socios/SocioEdit';
import TipoAplicacion from './containers/parametrizacion/tipo_aplicaciones/TipoAplicacion';
import TipoAplicacionAdd from './containers/parametrizacion/tipo_aplicaciones/TipoAplicacionAdd';
import TipoAplicacionEdit from './containers/parametrizacion/tipo_aplicaciones/TipoAplicacionEdit';
import TipoAlianza from './containers/parametrizacion/tipo_alianzas/TipoAlianza';
import TipoAlianzaAdd from './containers/parametrizacion/tipo_alianzas/TipoAlianzaAdd';
import TipoAlianzaEdit from './containers/parametrizacion/tipo_alianzas/TipoAlianzaEdit';
import Alianza from './containers/parametrizacion/alianzas/Alianza';
import AlianzaAdd from './containers/parametrizacion/alianzas/AlianzaAdd';
import AlianzaEdit from './containers/parametrizacion/alianzas/AlianzaEdit';
import TipoAhorro from './containers/parametrizacion/tipo_ahorros/TipoAhorro';
import TipoAhorroAdd from './containers/parametrizacion/tipo_ahorros/TipoAhorroAdd';
import TipoAhorroEdit from './containers/parametrizacion/tipo_ahorros/TipoAhorroEdit';
import Ahorro from './containers/parametrizacion/ahorros/Ahorro';
import AhorroAdd from './containers/parametrizacion/ahorros/AhorroAdd';
import AhorroEdit from './containers/parametrizacion/ahorros/AhorroEdit';
import Credito from './containers/parametrizacion/creditos_libre_inversion/Credito';
import CreditoAdd from './containers/parametrizacion/creditos_libre_inversion/CreditoAdd';
import CreditoEdit from './containers/parametrizacion/creditos_libre_inversion/CreditoEdit';
import Empleado from './containers/parametrizacion/empleados/Empleado';
import EmpleadoAdd from './containers/parametrizacion/empleados/EmpleadoAdd';
import EmpleadoEdit from './containers/parametrizacion/empleados/EmpleadoEdit';
import Organizacion from './containers/parametrizacion/organizaciones/Organizacion';
import OrganizacionAdd from './containers/parametrizacion/organizaciones/OrganizacionAdd';
import OrganizacionEdit from './containers/parametrizacion/organizaciones/OrganizacionEdit';
import Perfil from './containers/parametrizacion/perfiles/Perfil';
import PerfilAdd from './containers/parametrizacion/perfiles/PerfilAdd';
import PerfilEdit from './containers/parametrizacion/perfiles/PerfilEdit';
import Permiso from './containers/parametrizacion/permisos/Permiso';
import PermisoAdd from './containers/parametrizacion/permisos/PermisoAdd';
import PermisoEdit from './containers/parametrizacion/permisos/PermisoEdit';
import Opcion from './containers/parametrizacion/opciones/Opcion';
import OpcionAdd from './containers/parametrizacion/opciones/OpcionAdd';
import OpcionEdit from './containers/parametrizacion/opciones/OpcionEdit';

export default function Routes() {
  return (
    <Router>
      <Switch>
        <Route path="/" component={Login} exact ></Route>
        <Route path="/login" component={Login}></Route>
        <Route path="/seleccion" component={Seleccion}></Route>

        {/* Admin */}
        <Route path="/homeAdmin" component={HomeAdmin}></Route>


        {/* Parametrizacion */}
        <Route path="/homeParametrizacion" component={HomeParametrizacion}></Route>
        <Route path="/usuarios" component={Usuario}></Route>
        <Route path="/agregarUsuarios" component={UsuarioAdd}></Route>
        <Route path="/editarUsuarios" component={UsuarioEdit}></Route>
        <Route path="/socios" component={Socio}></Route>
        <Route path="/agregarSocios" component={SocioAdd}></Route>
        <Route path="/editarSocios" component={SocioEdit}></Route>
        <Route path="/tipoAplicacion" component={TipoAplicacion}></Route>
        <Route path="/agregarTipoAplicacion" component={TipoAplicacionAdd}></Route>
        <Route path="/editarTipoAplicacion" component={TipoAplicacionEdit}></Route>
        <Route path="/tipoAlianza" component={TipoAlianza}></Route>
        <Route path="/agregarTipoAlianza" component={TipoAlianzaAdd}></Route>
        <Route path="/editarTipoAlianza" component={TipoAlianzaEdit}></Route>
        <Route path="/alianza" component={Alianza}></Route>
        <Route path="/agregarAlianza" component={AlianzaAdd}></Route>
        <Route path="/editarAlianza" component={AlianzaEdit}></Route>
        <Route path="/tipoAhorro" component={TipoAhorro}></Route>
        <Route path="/agregarTipoAhorro" component={TipoAhorroAdd}></Route>
        <Route path="/editarTipoAhorro" component={TipoAhorroEdit}></Route>
        <Route path="/ahorro" component={Ahorro}></Route>
        <Route path="/agregarAhorro" component={AhorroAdd}></Route>
        <Route path="/editarAhorro" component={AhorroEdit}></Route>
        <Route path="/credito" component={Credito}></Route>
        <Route path="/agregarCredito" component={CreditoAdd}></Route>
        <Route path="/editarCredito" component={CreditoEdit}></Route>
        <Route path="/empleado" component={Empleado}></Route>
        <Route path="/agregarEmpleado" component={EmpleadoAdd}></Route>
        <Route path="/editarEmpleado" component={EmpleadoEdit}></Route>
        <Route path="/organizaciones" component={Organizacion}></Route>
        <Route path="/agregarOrganizaciones" component={OrganizacionAdd}></Route>
        <Route path="/editarOrganizaciones" component={OrganizacionEdit}></Route>
        <Route path="/perfiles" component={Perfil}></Route>
        <Route path="/agregarPerfiles" component={PerfilAdd}></Route>
        <Route path="/editarPerfiles" component={PerfilEdit}></Route>
        <Route path="/permisos" component={Permiso}></Route>
        <Route path="/agregarPermisos" component={PermisoAdd}></Route>
        <Route path="/editarPermisos" component={PermisoEdit}></Route>
        <Route path="/opciones" component={Opcion}></Route>
        <Route path="/agregarOpciones" component={OpcionAdd}></Route>
        <Route path="/editarOpciones" component={OpcionEdit}></Route>



      </Switch>
    </Router>
  );
}
